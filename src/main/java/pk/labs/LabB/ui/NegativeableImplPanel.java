package pk.labs.LabB.ui;

import javax.swing.JPanel;
import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Component;
import pk.labs.LabB.Contracts.ControlPanel;

@Component
public class NegativeableImplPanel implements Negativeable {
    Utils utils;
    
    public NegativeableImplPanel() {
        this.utils = new Utils();
    }

    @Override
    public void negative() {
        this.utils.negateComponent(((ControlPanel)AopContext.currentProxy()).getPanel());
        System.out.println("---> INTRODUCTION <--- CONTROL PANEL");
    }
}
