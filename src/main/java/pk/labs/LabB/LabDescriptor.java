package pk.labs.LabB;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "pk.labs.LabB.Impl.Display.DisplayBean";
    public static String controlPanelImplClassName = "pk.labs.LabB.Impl.ControlPanel.ControlPanelBean";

    public static String mainComponentSpecClassName = "pk.labs.LabB.Contracts.Microwave";
    public static String mainComponentImplClassName = "pk.labs.LabB.Impl.MainComponent.MicrowaveBean";
    public static String mainComponentBeanName = "microwaveBean";
    // endregion

    // region P2
    public static String mainComponentMethodName = "on";
    public static Object[] mainComponentMethodExampleParams = new Object[] { };
    // endregion

    // region P3
    public static String loggerAspectBeanName = "loggerAspect";
    // endregion
}
