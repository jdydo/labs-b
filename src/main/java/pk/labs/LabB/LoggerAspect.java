package pk.labs.LabB;

import org.aspectj.lang.JoinPoint;
import org.springframework.stereotype.Component;
import pk.labs.LabB.Contracts.Logger;

@Component
public class LoggerAspect {

    Logger logger;

    public LoggerAspect() {
        this.logger = new LoggerImpl();
    }

    public void logMethodEntrance(JoinPoint point) {
        this.logger.logMethodEntrance(point.getSignature().getName(), point.getArgs());
    }

    public void logMethodExit(JoinPoint point, Object returningValue) {
        this.logger.logMethodExit(point.getSignature().getName(), returningValue);
    }
}
